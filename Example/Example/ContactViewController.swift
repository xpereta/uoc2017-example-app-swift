//
//  ContactViewController.swift
//  Example
//
//  Created by Xavier Pereta on 15/02/2017.
//  Copyright © 2017 uoc. All rights reserved.
//

import UIKit
import MessageUI

class ContactViewController: UIViewController, MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func contactAction(_ sender: UIButton) {
        let picker = MFMailComposeViewController()
        picker.mailComposeDelegate = self;
        picker.setSubject("need help")
        picker.setMessageBody("Please, I need help", isHTML: false)
        self.present(picker, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true)
    }
}
