//
//  UOCWebViewController.swift
//  Example
//
//  Created by Xavier Pereta on 14/02/2017.
//  Copyright © 2017 uoc. All rights reserved.
//

import UIKit

class UOCWebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.loadRequest(URLRequest(url: URL(string: "http://www.uoc.edu")!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
