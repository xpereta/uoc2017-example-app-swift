//
//  StudentsTableTableViewController.swift
//  Example
//
//  Created by Xavier Pereta on 14/02/2017.
//  Copyright © 2017 uoc. All rights reserved.
//

import UIKit

class StudentsTableViewController: UITableViewController {
    var tableArray:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableArray = ["Marta Pérez","John doe","Enrique García","Fernando Fernández","Luis Rojas"]
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentCell") as? StudentCell
        
        let rowNumber = indexPath.row
        cell?.studentName?.text = tableArray[rowNumber]
        
        return cell!
    }
}
